package sh.surge.enksoftware.automatuum.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import sh.surge.enksoftware.automatuum.tileentity.OilRefinerTileEntity;

import javax.annotation.Nonnull;

public class OilRefinerContainer extends Container
{
    // General offsets and declarations
    public static final int xOffset = 8;
    public static final int yOffset = 84;
    public static final int tileYOffset = 41;
    public static final int slotBorder = 1;
    public static final int invSlotsVert = 3;
    public static final int invSlotsHorz = 9;
    public static final int hotbarSlotsHorz = 9;
    public static final int hotbarSepeartion = 4;

    // Input Slots (Crude Oil Bucket, Oil Refining Sieve Meshes)
    public static final int tileOilBucketIn = 44;
    public static final int tileOilRefiningSieveMeshIn = 26;

    // Output slot
    public static final int tileOilBucketOut = 127;

    // Output slot (y-offset)
    public static final int tileInvOilBucketOutYOffset = 7;

    // TileEntity
    public OilRefinerTileEntity blockTileEntity;

    // Ctr
    public OilRefinerContainer(EntityPlayer player, OilRefinerTileEntity tileEntity)
    {
        this.blockTileEntity = tileEntity;
        addPlayerInventory(player);

        if (blockTileEntity.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null))
        {
            addTileEntityInventory();
        }
    }

    // Add the player inventory to the container
    public void addPlayerInventory(EntityPlayer playerIn)
    {
        IInventory playerInv = playerIn.inventory;
        for (int row = 0; row < invSlotsVert; row++)
        {
            for (int col = 0; col < invSlotsHorz; col++)
            {
                int x = xOffset + col * (16 + (slotBorder * 2));
                int y = row * (16 + (slotBorder * 2)) + yOffset;

                this.addSlotToContainer(
                        new Slot(
                                playerInv, col + row * invSlotsHorz + hotbarSlotsHorz, x, y
                        )
                );
            }
        }

        int hotbarY = (invSlotsVert * (16 + (slotBorder * 2))) + hotbarSepeartion + yOffset;

        for (int col = 0; col < invSlotsHorz; col++)
        {
            int x = xOffset + col * (16 + (slotBorder * 2));
            this.addSlotToContainer(
                    new Slot(
                            playerInv, col, x, hotbarY
                    )
            );
        }
    }

    // Add the TileEntity Inventory to the container
    public void addTileEntityInventory()
    {
        IItemHandler inv = blockTileEntity.getCapability(
                CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null
        );

        // Add the two input slots
        addSlotToContainer(
                new SlotItemHandler(
                        inv, 0, tileOilBucketIn, tileYOffset
                )
        );
        addSlotToContainer(
                new SlotItemHandler(
                        inv, 1, tileOilRefiningSieveMeshIn, tileYOffset
                )
        );

        // Add the output slot and make it unusable to insert an item there
        addSlotToContainer(new SlotItemHandler(inv, 2, tileOilBucketOut, tileYOffset - tileInvOilBucketOutYOffset)
        {
            @Override
            public boolean isItemValid(@Nonnull ItemStack stack)
            {
                return false;
            }
        });
    }

    // Can the player interact with the container?

    @Override
    public boolean canInteractWith(EntityPlayer playerIn)
    {
        return blockTileEntity.canInteractWith(playerIn);
    }

    // Transfer Items into slot... (Not working, but crash circumvention)
    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
    {
        ItemStack itemStack = ItemStack.EMPTY;
        Slot slot = inventorySlots.get(index);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemStack1 = slot.getStack();
            itemStack = itemStack1.copy();

            int containerSlots = inventorySlots.size() - playerIn.inventory.mainInventory.size();

            if (index < containerSlots)
            {
                if (!this.mergeItemStack(itemStack1, containerSlots, inventorySlots.size(), false))
                {
                    return ItemStack.EMPTY;
                }
            }
            else if (!this.mergeItemStack(itemStack1, 0, containerSlots, false))
            {
                return ItemStack.EMPTY;
            }

            if (itemStack1.isEmpty())
            {
                slot.putStack(ItemStack.EMPTY);
            }
            else
            {
                slot.onSlotChanged();
            }

            if (itemStack1.getCount() == itemStack.getCount())
            {
                return ItemStack.EMPTY;
            }

            slot.onTake(playerIn, itemStack1);
        }

        return itemStack;
    }
}
