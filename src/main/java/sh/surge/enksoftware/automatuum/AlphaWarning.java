package sh.surge.enksoftware.automatuum;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(modid = "automatuum", value = Side.CLIENT)
public class AlphaWarning
{
    static boolean sent = false;
    static Minecraft mc = Minecraft.getMinecraft();

    @SubscribeEvent
    public static void onEntityJoin(EntityJoinWorldEvent event)
    {
        if (AutomatuumConfig.GENERAL.alphaWarningSender)
        {
            if (event.getWorld().isRemote && event.getEntity() instanceof EntityPlayer && !sent)
            {
                mc.player.sendMessage(new TextComponentTranslation("automatuum.alpha_warn"));
                sent = true;
            }
        }
    }
}
