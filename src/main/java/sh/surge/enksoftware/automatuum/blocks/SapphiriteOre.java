package sh.surge.enksoftware.automatuum.blocks;

import sh.surge.enksoftware.automatuum.AutomatuumTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class SapphiriteOre extends Block
{
    public SapphiriteOre()
    {
        super(Material.ROCK);
        setSoundType(SoundType.STONE);
        setHardness(6.0F);
        setResistance(9.5F);
        setHarvestLevel("pickaxe", 3);
        setRegistryName("sapphirite_ore");
        setTranslationKey("automatuum.sapphirite_ore");
        setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
    }
}
