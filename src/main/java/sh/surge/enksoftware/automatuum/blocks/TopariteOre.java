package sh.surge.enksoftware.automatuum.blocks;

import sh.surge.enksoftware.automatuum.AutomatuumTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class TopariteOre extends Block
{
    public TopariteOre()
    {
        super(Material.ROCK);
        setSoundType(SoundType.STONE);
        setHardness(6.75F);
        setResistance(11.5F);
        setHarvestLevel("pickaxe", 5);
        setRegistryName("toparite_ore");
        setTranslationKey("automatuum.toparite_ore");
        setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
    }
}
