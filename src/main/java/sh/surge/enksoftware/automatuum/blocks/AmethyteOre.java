package sh.surge.enksoftware.automatuum.blocks;

import sh.surge.enksoftware.automatuum.AutomatuumTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class AmethyteOre extends Block
{
    public AmethyteOre()
    {
        super(Material.ROCK);
        setSoundType(SoundType.STONE);
        setHardness(7.5F);
        setResistance(14.0F);
        setHarvestLevel("pickaxe", 6);
        setRegistryName("amethyte_ore");
        setTranslationKey("automatuum.amethyte_ore");
        setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
    }
}
