package sh.surge.enksoftware.automatuum.blocks;

import sh.surge.enksoftware.automatuum.AutomatuumTab;
import sh.surge.enksoftware.automatuum.tileentity.UniversalCableTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.energy.CapabilityEnergy;

import javax.annotation.Nullable;

public class TopariteCable extends Block
{
    // Connection properties for North, South, East, West, Up and Down
    public static final PropertyBool NORTH = PropertyBool.create("north");
    public static final PropertyBool SOUTH = PropertyBool.create("south");
    public static final PropertyBool EAST = PropertyBool.create("east");
    public static final PropertyBool WEST = PropertyBool.create("west");
    public static final PropertyBool UP = PropertyBool.create("up");
    public static final PropertyBool DOWN = PropertyBool.create("down");

    public TopariteCable()
    {
        super(Material.IRON);
        setSoundType(SoundType.METAL);
        setHardness(9.75F);
        setResistance(39.5F);
        setHarvestLevel("pickaxe", 5);
        setRegistryName("toparite_cable");
        setTranslationKey("automatuum.toparite_cable");
        setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
        setDefaultState(getBlockState().getBaseState().withProperty(NORTH, false).withProperty(SOUTH, false)
                .withProperty(EAST, false).withProperty(WEST, false).withProperty(UP, false).withProperty(DOWN, false));
    }

    @Override
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
        return true;
    }

    @Override
    public int getMetaFromState(IBlockState state)
    {
        return 0;
    }

    // Create the blockstate

    @Override
    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, NORTH, SOUTH, EAST, WEST, UP, DOWN);
    }

    // Get the actual state

    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos)
    {
        return state.withProperty(NORTH, canCableConnectTo(worldIn, pos, EnumFacing.NORTH))
                .withProperty(SOUTH, canCableConnectTo(worldIn, pos, EnumFacing.SOUTH))
                .withProperty(EAST, canCableConnectTo(worldIn, pos, EnumFacing.EAST))
                .withProperty(WEST, canCableConnectTo(worldIn, pos, EnumFacing.WEST))
                .withProperty(UP, canCableConnectTo(worldIn, pos, EnumFacing.UP))
                .withProperty(DOWN, canCableConnectTo(worldIn, pos, EnumFacing.DOWN));
    }

    private boolean canCableConnectTo(IBlockAccess world, BlockPos pos, EnumFacing facing)
    {
        TileEntity dest = world.getTileEntity(pos.offset(facing));
        return dest != null && dest.hasCapability(CapabilityEnergy.ENERGY, facing.getOpposite());
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face)
    {
        return BlockFaceShape.UNDEFINED;
    }

    @Override
    public boolean hasTileEntity(IBlockState state)
    {
        return true;
    }

    @Nullable
    @Override
    public TileEntity createTileEntity(World world, IBlockState state)
    {
        return new UniversalCableTileEntity.TopariteCableTileEntity();
    }
}
