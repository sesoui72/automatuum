package sh.surge.enksoftware.automatuum.blocks;

import sh.surge.enksoftware.automatuum.Automatuum;
import sh.surge.enksoftware.automatuum.AutomatuumTab;
import sh.surge.enksoftware.automatuum.tileentity.*;
import sh.surge.enksoftware.automatuum.tileentity.tesr.BarrelTESR;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemSlab;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@GameRegistry.ObjectHolder("automatuum")
@Mod.EventBusSubscriber(modid = "automatuum")
public class BlockRegistry
{
    public static final Block SAPPHIRITE_ORE = Blocks.AIR;
    public static final Block HEARANIUM_ORE = Blocks.AIR;
    public static final Block TOPARITE_ORE = Blocks.AIR;
    public static final Block AMETHYTE_ORE = Blocks.AIR;
    public static final Block LARANIUM_ORE = Blocks.AIR;
    public static final Block CRYSTAL_ORE = Blocks.AIR;
    public static final Block FEROS_ORE = Blocks.AIR;

    // Blocks out of ingots
    public static final Block SAPPHIRITE_BLOCK = Blocks.AIR;
    public static final Block HEARANIUM_BLOCK = Blocks.AIR;
    public static final Block TOPARITE_BLOCK = Blocks.AIR;
    public static final Block AMETHYTE_BLOCK = Blocks.AIR;
    public static final Block LARANIUM_BLOCK = Blocks.AIR;
    public static final Block CRYSTAL_BLOCK = Blocks.AIR;
    public static final Block FEROS_BLOCK = Blocks.AIR;

    // Slabs
    public static final Block METAL_SLAB = Blocks.AIR;
    public static final Block DOUBLE_METAL_SLAB = Blocks.AIR;

    // Barrels
    public static final Block WOODEN_BARREL = Blocks.AIR;
    public static final Block STONE_BARREL = Blocks.AIR;
    public static final Block IRON_BARREL = Blocks.AIR;
    public static final Block GOLD_BARREL = Blocks.AIR;
    public static final Block DIAMOND_BARREL = Blocks.AIR;

    // Cable
    public static final Block TOPARITE_CABLE = Blocks.AIR;

    // Machines
    public static final Block CRUDE_OIL_GEN = Blocks.AIR;
    public static final Block REFINED_OIL_GEN = Blocks.AIR;
    public static final Block HIGH_FURNACE = Blocks.AIR;
    public static final Block OIL_REFINER = Blocks.AIR;

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event)
    {
        event.getRegistry().register(new LaraniumOre());
        event.getRegistry().register(new AmethyteOre());
        event.getRegistry().register(new TopariteOre());
        event.getRegistry().register(new HearaniumOre());
        event.getRegistry().register(new SapphiriteOre());
        event.getRegistry().register(new CrystalOre());
        event.getRegistry().register(new FerosOre());

        event.getRegistry().register(new LaraniumBlock());
        event.getRegistry().register(new AmethyteBlock());
        event.getRegistry().register(new TopariteBlock());
        event.getRegistry().register(new HearaniumBlock());
        event.getRegistry().register(new SapphiriteBlock());
        event.getRegistry().register(new CrystalBlock());
        event.getRegistry().register(new FerosBlock());

        event.getRegistry().register(new BlockMetalSlab.Half().setRegistryName("metal_slab"));
        event.getRegistry().register(new BlockMetalSlab.Double().setRegistryName("double_metal_slab"));

        // Tile Entities
        GameRegistry.registerTileEntity(CrystalBlockTileEntity.class, "automatuum:crystal_block_tile_entity");
        GameRegistry.registerTileEntity(BarrelTileEntity.WoodenBarrelTileEntity.class, "automatuum:w_barrel_tile_entity");
        GameRegistry.registerTileEntity(BarrelTileEntity.StoneBarrelTileEntity.class, "automatuum:s_barrel_tile_entity");
        GameRegistry.registerTileEntity(BarrelTileEntity.IronBarrelTileEntity.class, "automatuum:i_barrel_tile_entity");
        GameRegistry.registerTileEntity(BarrelTileEntity.GoldBarrelTileEntity.class, "automatuum:g_barrel_tile_entity");
        GameRegistry.registerTileEntity(BarrelTileEntity.DiamondBarrelTileEntity.class, "automatuum:d_barrel_tile_entity");
        GameRegistry.registerTileEntity(UniversalGeneratorTileEntity.CrudeOilGeneratorTE.class, "automatuum:crude_oil_generator_tile_entity");
        GameRegistry.registerTileEntity(UniversalGeneratorTileEntity.RefinedOilGeneratorTE.class, "automatuum:refined_oil_generator_tile_entity");
        GameRegistry.registerTileEntity(UniversalCableTileEntity.TopariteCableTileEntity.class, "automatuum:toparite_cable_tile_entity");
        GameRegistry.registerTileEntity(ElectricBlastFurnaceTileEntity.class, "automatuum:high_furnace_tile_entity");
        GameRegistry.registerTileEntity(OilRefinerTileEntity.class, "automatuum:oil_refiner_tile_entity");

        event.getRegistry().register(new TopariteCable());
        event.getRegistry().register(new CrudeGenerator());
        event.getRegistry().register(new RefinedGenerator());
        event.getRegistry().register(new ElectricalBlastFurnace());
        event.getRegistry().register(new OilRefiner());
    }

    @SubscribeEvent
    public static void registerItemBlocks(RegistryEvent.Register<Item> event)
    {
        event.getRegistry().register(new ItemBlock(LARANIUM_ORE).setRegistryName(LARANIUM_ORE.getRegistryName()));
        event.getRegistry().register(new ItemBlock(AMETHYTE_ORE).setRegistryName(AMETHYTE_ORE.getRegistryName()));
        event.getRegistry().register(new ItemBlock(TOPARITE_ORE).setRegistryName(TOPARITE_ORE.getRegistryName()));
        event.getRegistry().register(new ItemBlock(HEARANIUM_ORE).setRegistryName(HEARANIUM_ORE.getRegistryName()));
        event.getRegistry().register(new ItemBlock(SAPPHIRITE_ORE).setRegistryName(SAPPHIRITE_ORE.getRegistryName()));
        event.getRegistry().register(new ItemBlock(CRYSTAL_ORE).setRegistryName(CRYSTAL_ORE.getRegistryName()));
        event.getRegistry().register(new ItemBlock(FEROS_ORE).setRegistryName(FEROS_ORE.getRegistryName()));

        event.getRegistry().register(new ItemBlock(LARANIUM_BLOCK).setRegistryName(LARANIUM_BLOCK.getRegistryName()));
        event.getRegistry().register(new ItemBlock(AMETHYTE_BLOCK).setRegistryName(AMETHYTE_BLOCK.getRegistryName()));
        event.getRegistry().register(new ItemBlock(TOPARITE_BLOCK).setRegistryName(TOPARITE_BLOCK.getRegistryName()));
        event.getRegistry().register(new ItemBlock(HEARANIUM_BLOCK).setRegistryName(HEARANIUM_BLOCK.getRegistryName()));
        event.getRegistry().register(new ItemBlock(SAPPHIRITE_BLOCK).setRegistryName(SAPPHIRITE_BLOCK.getRegistryName()));
        event.getRegistry().register(new ItemBlock(CRYSTAL_BLOCK).setRegistryName(CRYSTAL_BLOCK.getRegistryName()));
        event.getRegistry().register(new ItemBlock(FEROS_BLOCK).setRegistryName(FEROS_BLOCK.getRegistryName()));

        if (METAL_SLAB instanceof BlockSlab && DOUBLE_METAL_SLAB instanceof BlockSlab)
        {
            event.getRegistry().register(new ItemSlab(METAL_SLAB, (BlockSlab) METAL_SLAB, (BlockSlab) DOUBLE_METAL_SLAB).setRegistryName(METAL_SLAB.getRegistryName()));
        }

        event.getRegistry().register(new ItemBlock(WOODEN_BARREL).setRegistryName(WOODEN_BARREL.getRegistryName()));
        event.getRegistry().register(new ItemBlock(STONE_BARREL).setRegistryName(STONE_BARREL.getRegistryName()));
        event.getRegistry().register(new ItemBlock(IRON_BARREL).setRegistryName(IRON_BARREL.getRegistryName()));
        event.getRegistry().register(new ItemBlock(GOLD_BARREL).setRegistryName(GOLD_BARREL.getRegistryName()));
        event.getRegistry().register(new ItemBlock(DIAMOND_BARREL).setRegistryName(DIAMOND_BARREL.getRegistryName()));

        event.getRegistry().register(new ItemBlock(TOPARITE_CABLE).setRegistryName(TOPARITE_CABLE.getRegistryName()));
        event.getRegistry().register(new ItemBlock(CRUDE_OIL_GEN).setRegistryName(CRUDE_OIL_GEN.getRegistryName()));
        event.getRegistry().register(new ItemBlock(REFINED_OIL_GEN).setRegistryName(REFINED_OIL_GEN.getRegistryName()));
        event.getRegistry().register(new ItemBlock(HIGH_FURNACE).setRegistryName(HIGH_FURNACE.getRegistryName()));
        event.getRegistry().register(new ItemBlock(OIL_REFINER).setRegistryName(OIL_REFINER.getRegistryName()));
    }

    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    public static void registerItemBlockModels(ModelRegistryEvent event)
    {
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(LARANIUM_ORE), 0, new ModelResourceLocation(LARANIUM_ORE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(AMETHYTE_ORE), 0, new ModelResourceLocation(AMETHYTE_ORE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(TOPARITE_ORE), 0, new ModelResourceLocation(TOPARITE_ORE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(HEARANIUM_ORE), 0, new ModelResourceLocation(HEARANIUM_ORE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(SAPPHIRITE_ORE), 0, new ModelResourceLocation(SAPPHIRITE_ORE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(CRYSTAL_ORE), 0, new ModelResourceLocation(CRYSTAL_ORE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(FEROS_ORE), 0, new ModelResourceLocation(FEROS_ORE.getRegistryName(), "inventory"));

        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(LARANIUM_BLOCK), 0, new ModelResourceLocation(LARANIUM_BLOCK.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(AMETHYTE_BLOCK), 0, new ModelResourceLocation(AMETHYTE_BLOCK.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(TOPARITE_BLOCK), 0, new ModelResourceLocation(TOPARITE_BLOCK.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(HEARANIUM_BLOCK), 0, new ModelResourceLocation(HEARANIUM_BLOCK.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(SAPPHIRITE_BLOCK), 0, new ModelResourceLocation(SAPPHIRITE_BLOCK.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(CRYSTAL_BLOCK), 0, new ModelResourceLocation(CRYSTAL_BLOCK.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(FEROS_BLOCK), 0, new ModelResourceLocation(FEROS_BLOCK.getRegistryName(), "inventory"));

        Item metalSlab = Item.getItemFromBlock(METAL_SLAB);
        for (BlockMetalSlab.EnumType variant : BlockMetalSlab.EnumType.values())
        {
            ModelLoader.setCustomModelResourceLocation(metalSlab, variant.getMetadata(), new ModelResourceLocation(metalSlab.getRegistryName(), "half=bottom,variant=" + variant.getName()));
        }

        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(WOODEN_BARREL), 0, new ModelResourceLocation(WOODEN_BARREL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(STONE_BARREL), 0, new ModelResourceLocation(STONE_BARREL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(IRON_BARREL), 0, new ModelResourceLocation(IRON_BARREL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(GOLD_BARREL), 0, new ModelResourceLocation(GOLD_BARREL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(DIAMOND_BARREL), 0, new ModelResourceLocation(DIAMOND_BARREL.getRegistryName(), "inventory"));

        // Renderer thing here.
        ClientRegistry.bindTileEntitySpecialRenderer(BarrelTileEntity.WoodenBarrelTileEntity.class, new BarrelTESR());
        ClientRegistry.bindTileEntitySpecialRenderer(BarrelTileEntity.StoneBarrelTileEntity.class, new BarrelTESR());
        ClientRegistry.bindTileEntitySpecialRenderer(BarrelTileEntity.IronBarrelTileEntity.class, new BarrelTESR());
        ClientRegistry.bindTileEntitySpecialRenderer(BarrelTileEntity.GoldBarrelTileEntity.class, new BarrelTESR());
        ClientRegistry.bindTileEntitySpecialRenderer(BarrelTileEntity.DiamondBarrelTileEntity.class, new BarrelTESR());

        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(TOPARITE_CABLE), 0, new ModelResourceLocation(TOPARITE_CABLE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(CRUDE_OIL_GEN), 0, new ModelResourceLocation(CRUDE_OIL_GEN.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(REFINED_OIL_GEN), 0, new ModelResourceLocation(REFINED_OIL_GEN.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(HIGH_FURNACE), 0, new ModelResourceLocation(HIGH_FURNACE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(OIL_REFINER), 0, new ModelResourceLocation(OIL_REFINER.getRegistryName(), "inventory"));
    }

    @SubscribeEvent
    public static void registerAllBarrels(RegistryEvent.Register<Block> event)
    {
        event.getRegistry().registerAll(
                setHarvestLevel(
                        new BlockBarrel(SoundType.WOOD, Material.WOOD)
                        {
                            @Override
                            public TileEntity createTileEntity(World world, IBlockState state)
                            {
                                return new BarrelTileEntity.WoodenBarrelTileEntity();
                            }
                        }
                        .setRegistryName("wooden_barrel")
                        .setTranslationKey(Automatuum.MOD_ID + ".wooden_barrel")
                        .setHardness(3.5F)
                        .setResistance(7.5F)
                        .setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB)
                        .setLightOpacity(0),
                        "axe", 0
                ),

                setHarvestLevel(
                        new BlockBarrel(SoundType.STONE, Material.ROCK)
                        {
                            @Override
                            public TileEntity createTileEntity(World world, IBlockState state)
                            {
                                return new BarrelTileEntity.StoneBarrelTileEntity();
                            }
                        }
                        .setRegistryName("stone_barrel")
                        .setTranslationKey(Automatuum.MOD_ID + ".stone_barrel")
                        .setHardness(6.0F)
                        .setResistance(10.0F)
                        .setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB)
                        .setLightOpacity(0),
                        "pickaxe", 0
                ),

                setHarvestLevel(
                        new BlockBarrel(SoundType.METAL, Material.IRON)
                        {
                            @Override
                            public TileEntity createTileEntity(World world, IBlockState state)
                            {
                                return new BarrelTileEntity.IronBarrelTileEntity();
                            }
                        }
                        .setRegistryName("iron_barrel")
                        .setTranslationKey(Automatuum.MOD_ID + ".iron_barrel")
                        .setHardness(10.0F)
                        .setResistance(12.5F)
                        .setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB)
                        .setLightOpacity(0),
                        "pickaxe", 1
                ),

                setHarvestLevel(
                        new BlockBarrel(SoundType.METAL, Material.IRON)
                        {
                            @Override
                            public TileEntity createTileEntity(World world, IBlockState state)
                            {
                                return new BarrelTileEntity.GoldBarrelTileEntity();
                            }
                        }
                        .setRegistryName("gold_barrel")
                        .setTranslationKey(Automatuum.MOD_ID + ".gold_barrel")
                        .setHardness(8.5F)
                        .setResistance(11.5F)
                        .setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB)
                        .setLightOpacity(0),
                        "pickaxe", 2
                ),

                setHarvestLevel(
                        new BlockBarrel(SoundType.METAL, Material.IRON)
                        {
                            @Override
                            public TileEntity createTileEntity(World world, IBlockState state)
                            {
                                return new BarrelTileEntity.DiamondBarrelTileEntity();
                            }
                        }
                        .setRegistryName("diamond_barrel")
                        .setTranslationKey(Automatuum.MOD_ID + ".diamond_barrel")
                        .setHardness(13.0F)
                        .setResistance(15.0F)
                        .setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB)
                        .setLightOpacity(0),
                        "pickaxe", 2
                )
        );
    }

    public static Block setHarvestLevel(Block block, String toolClass, int level)
    {
        block.setHarvestLevel(toolClass, level);
        return block;
    }
}
