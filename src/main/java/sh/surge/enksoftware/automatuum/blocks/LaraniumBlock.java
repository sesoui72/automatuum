package sh.surge.enksoftware.automatuum.blocks;

import sh.surge.enksoftware.automatuum.AutomatuumTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class LaraniumBlock extends Block
{
    public LaraniumBlock()
    {
        super(Material.IRON);
        setSoundType(SoundType.METAL);
        setHardness(13.0F);
        setResistance(44.0F);
        setHarvestLevel("pickaxe", 7);
        setRegistryName("laranium_block");
        setTranslationKey("automatuum.laranium_block");
        setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
    }
}
