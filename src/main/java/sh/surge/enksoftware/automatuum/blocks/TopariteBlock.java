package sh.surge.enksoftware.automatuum.blocks;

import sh.surge.enksoftware.automatuum.AutomatuumTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class TopariteBlock extends Block
{
    public TopariteBlock()
    {
        super(Material.IRON);
        setSoundType(SoundType.METAL);
        setHardness(11.75F);
        setResistance(41.5F);
        setHarvestLevel("pickaxe", 5);
        setRegistryName("toparite_block");
        setTranslationKey("automatuum.toparite_block");
        setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
    }
}
