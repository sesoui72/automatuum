package sh.surge.enksoftware.automatuum.util;


import net.minecraft.item.Item;
import net.minecraftforge.common.util.EnumHelper;

public class MatRefs
{
    // Blocks
    public static final Item.ToolMaterial FEROS = EnumHelper.addToolMaterial("automatuum:feros", 9, 2375, 7.5F, 5.5F, 26);
    public static final Item.ToolMaterial LARANIUM = EnumHelper.addToolMaterial("automatuum:laranium", 8, 2000, 8.0F, 5.325F, 23);
    public static final Item.ToolMaterial AMETHYTE = EnumHelper.addToolMaterial("automatuum:amethyte", 7, 1900, 7.0F, 5.0F, 19);
    public static final Item.ToolMaterial TOPARITE = EnumHelper.addToolMaterial("automatuum:toparite", 6, 1750, 6.0F, 4.6F, 16);
    public static final Item.ToolMaterial HEARANIUM = EnumHelper.addToolMaterial("automatuum:hearanium", 5, 1500, 7.5F, 4.0F, 15);
    public static final Item.ToolMaterial SAPPHIRITE = EnumHelper.addToolMaterial("automatuum:sapphirite", 4, 1350, 7.0F, 3.5F, 13);
}