package sh.surge.enksoftware.automatuum.tileentity.tesr;

import sh.surge.enksoftware.automatuum.util.RenderUtil;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.client.model.animation.FastTESR;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public class BarrelTESR extends FastTESR<TileEntity>
{
    @Override
    public void renderTileEntityFast(TileEntity te, double x, double y, double z, float partialTicks, int destroyStage, float partial, BufferBuilder buffer)
    {
        if (te.isInvalid() || !te.hasCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, null))
            return;

        IFluidTankProperties[] barrels = te.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, null).getTankProperties();
        if (barrels.length == 0)
            return;

        IFluidTankProperties barrel = barrels[0];
        FluidStack fluid = barrel.getContents();

        if (fluid != null)
        {
            float height = ((float)fluid.amount) / ((float)barrel.getCapacity());

            float offset = 0.9F / 16.0F;
            float tankBottom = offset;
            float tankTop = 1.0F - offset;

            buffer.setTranslation(x, y, z);
            RenderUtil.renderFluidCuboid(buffer, fluid, te.getPos(), offset, tankBottom, offset, 1.0D - offset, (height * (tankTop - tankBottom)) + tankBottom, 1.0D - offset);
        }
    }
}
