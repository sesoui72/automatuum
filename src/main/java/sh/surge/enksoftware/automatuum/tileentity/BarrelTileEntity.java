package sh.surge.enksoftware.automatuum.tileentity;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;

import javax.annotation.Nullable;

public class BarrelTileEntity extends TileEntity
{
    public FluidTank fluidTank;

    // Construct a tile entity object with capacity as parameter which is being passed on
    public BarrelTileEntity(int capacity)
    {
        fluidTank = new FluidTank(capacity)
        {
            @Override
            public void onContentsChanged()
            {
                BarrelTileEntity.this.markDirty();
            }

            @Override
            public boolean canFillFluidType(FluidStack fluid)
            {
                return !fluid.getFluid().isGaseous(fluid);
            }
        };
    }

    // NBT
    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        fluidTank.readFromNBT(compound);
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound)
    {
        super.writeToNBT(compound);
        fluidTank.writeToNBT(compound);
        return compound;
    }

    // Capability
    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing)
    {
        return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing)
    {
        if (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
        {
            return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(fluidTank);
        }

        return super.getCapability(capability, facing);
    }

    // Syncing
    @Override
    @Nullable
    public SPacketUpdateTileEntity getUpdatePacket()
    {
        return new SPacketUpdateTileEntity(getPos(), 0, getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
    {
        handleUpdateTag(pkt.getNbtCompound());
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag)
    {
        super.handleUpdateTag(tag);
        fluidTank.readFromNBT(tag);
        // Other syncable stuff goes here
    }

    @Override
    public NBTTagCompound getUpdateTag()
    {
        NBTTagCompound tag = super.getUpdateTag();
        tag = fluidTank.writeToNBT(tag);
        // Other syncable stuff goes here...
        return tag;
    }

    public static class WoodenBarrelTileEntity extends BarrelTileEntity
    {
        public WoodenBarrelTileEntity()
        {
            super(10000);
        }
    }

    public static class StoneBarrelTileEntity extends BarrelTileEntity
    {
        public StoneBarrelTileEntity()
        {
            super(15000);
        }
    }

    public static class IronBarrelTileEntity extends BarrelTileEntity
    {
        public IronBarrelTileEntity()
        {
            super(20000);
        }
    }

    public static class GoldBarrelTileEntity extends BarrelTileEntity
    {
        public GoldBarrelTileEntity()
        {
            super(25000);
        }
    }

    public static class DiamondBarrelTileEntity extends BarrelTileEntity
    {
        public DiamondBarrelTileEntity()
        {
            super(30000);
        }
    }
}
