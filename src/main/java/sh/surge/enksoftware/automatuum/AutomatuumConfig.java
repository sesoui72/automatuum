package sh.surge.enksoftware.automatuum;

import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = Automatuum.MOD_ID)
@Config(modid = "automatuum", category = "")
public class AutomatuumConfig
{
    // Config Fields here
    @Config.LangKey("automatuum.config.general")
    public static final General GENERAL = new General();

    @Config.LangKey("automatuum.config.oregen")
    public static final OreGen OREGEN = new OreGen();

    @Config.LangKey("automatuum.config.fluidgen")
    public static final FluidGen FLUIDGEN = new FluidGen();

    @Config.LangKey("automatuum.config.experimental")
    public static final Experimental EXPERIMENTAL = new Experimental();

    // Config classes
    public static class General
    {
        // This subclass is for the modification of certain general things.

        @Config.Comment("Checks whether or not we should log the generation of ores or custom fluid lakes.")
        public boolean oreFluidLogger = true;

        @Config.Comment("Checks whether or not we should send the alpha warning when joining a world.")
        public boolean alphaWarningSender = true;
    }

    public static class OreGen
    {
        // This subclass is for the generation of custom ores.

        @Config.Comment("Should Laranium Ore be generated?")
        public boolean oreGenLaranium = true;

        @Config.Comment("Should Crystal Ore be generated?")
        public boolean oreGenCrystal = true;

        @Config.Comment("Should Hearanium Ore be generated?")
        public boolean oreGenHearanium = true;

        @Config.Comment("Should Sapphirite Ore be generated?")
        public boolean oreGenSapphirite = true;

        @Config.Comment("Should Amethyte Ore be generated?")
        public boolean oreGenAmethyte = true;

        @Config.Comment("Should Toparite Ore be generated?")
        public boolean oreGenToparite = true;

        @Config.Comment("Should Feros Ore be generated?")
        public boolean oreGenFeros = true;
    }

    public static class FluidGen
    {
        // This subclass is for custom fluid lake generation

        @Config.Comment("Should Crude Oil Yields be generated?")
        public boolean fluidGenCrudeOil = true;
    }

    public static class Experimental
    {
        // Add whatever experimental, non-alpha planned features here.
    }

    @SubscribeEvent
    public static void onConfigChanged(final ConfigChangedEvent.OnConfigChangedEvent event)
    {
        if (event.getModID().equals(Automatuum.MOD_ID))
        {
            ConfigManager.sync(Automatuum.MOD_ID, Config.Type.INSTANCE);
        }
    }
}
