package sh.surge.enksoftware.automatuum.client.gui;

import sh.surge.enksoftware.automatuum.Automatuum;
import sh.surge.enksoftware.automatuum.inventory.RefinedGenContainer;
import sh.surge.enksoftware.automatuum.tileentity.UniversalGeneratorTileEntity;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

import java.text.NumberFormat;
import java.util.ArrayList;

public class RefinedGenGuiContainer extends GuiContainer
{
    private static final ResourceLocation CONTAINER_TEXTURE = new ResourceLocation(Automatuum.MOD_ID, "textures/gui/container/refined_generator.png");

    private IInventory playerInventory;
    private UniversalGeneratorTileEntity.RefinedOilGeneratorTE tileEntity;

    public RefinedGenGuiContainer(EntityPlayer player, UniversalGeneratorTileEntity.RefinedOilGeneratorTE tileEntity)
    {
        super(new RefinedGenContainer(player, tileEntity));
        this.playerInventory = player.inventory;
        this.tileEntity = tileEntity;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks)
    {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
    {
        String s = "Refined Oil Energy Generator";
        this.fontRenderer.drawString(s, this.xSize / 2 - this.fontRenderer.getStringWidth(s) / 2, 26, 0x404040);
        this.fontRenderer.drawString(this.playerInventory.getDisplayName().getUnformattedText(), 8, this.ySize - 96 + 2, 0x404040);

        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;

        NumberFormat numFormat = NumberFormat.getInstance();

        if (this.isPointInRegion(6, 5, 170 - 6, 19 - 5, mouseX, mouseY))
        {
            if (tileEntity.hasCapability(CapabilityEnergy.ENERGY, null))
            {
                IEnergyStorage storage = tileEntity.getCapability(CapabilityEnergy.ENERGY, null);
                ArrayList<String> text = new ArrayList<>();
                text.add(I18n.format("automatuum.refined_oil_gen.energy", numFormat.format(storage.getEnergyStored()), numFormat.format(storage.getMaxEnergyStored())));
                this.drawHoveringText(text, mouseX - i, mouseY - j);
            }
        }

        if (this.isPointInRegion(84, 41, 11, 17, mouseX, mouseY))
        {
            ArrayList<String> text = new ArrayList<>();
            String fluidName = null;
            if (tileEntity.hasCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, null))
            {
                IFluidTankProperties[] tanks = tileEntity.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, null).getTankProperties();
                FluidStack fluid = tanks.length > 0 ? tanks[0].getContents() : null;
                if (fluid != null)
                {
                    fluidName = fluid.getLocalizedName();
                }
            }

            if (fluidName == null)
            {
                text.add(I18n.format("automatuum.refined_oil_gen.tank.empty"));
            }
            else
            {
                text.add(I18n.format("automatuum.refined_oil_gen.tank", fluidName, (int)(getFluidLeft() * 100.0F)));
            }
            this.drawHoveringText(text, mouseX - i, mouseY - j);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
    {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(CONTAINER_TEXTURE);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);

        int l = this.getEnergyLeftScaled(162);
        if (l > 0)
        {
            this.drawTexturedModalRect(i + 8, j + 6, 0, 166, l, 11);
        }

        int m = this.getFluidLeftScaled(17);
        if (m > 0)
        {
            this.drawTexturedModalRect(i + 84, j + 41 + 17 - m, 176, 33 - m, 11, m + 1);
        }
    }

    private int getEnergyLeftScaled(int pixels)
    {
        if (tileEntity.hasCapability(CapabilityEnergy.ENERGY, null))
        {
            IEnergyStorage storage = tileEntity.getCapability(CapabilityEnergy.ENERGY, null);
            float i = storage.getEnergyStored();
            float j = storage.getMaxEnergyStored();
            return j != 0.0F && i != 0.0F ? (int) (i * pixels / j) : 0;
        }
        return 0;
    }

    private int getFluidLeftScaled(int pixels)
    {
        return (int) (getFluidLeft() * pixels);
    }

    private float getFluidLeft()
    {
        if (tileEntity.hasCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, null))
        {
            IFluidTankProperties[] tanks = tileEntity.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, null).getTankProperties();
            FluidStack fluid = tanks.length > 0 ? tanks[0].getContents() : null;
            if (fluid != null)
            {
                float i = fluid.amount;
                float j = this.tileEntity.fluidBuffer.getCapacity();
                return j != 0.0F && i != 0.0F ? i / j : 0;
            }
        }
        return 0;
    }
}
