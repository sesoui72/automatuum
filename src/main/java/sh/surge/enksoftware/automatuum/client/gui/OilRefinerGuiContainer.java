package sh.surge.enksoftware.automatuum.client.gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import sh.surge.enksoftware.automatuum.Automatuum;
import sh.surge.enksoftware.automatuum.inventory.OilRefinerContainer;
import sh.surge.enksoftware.automatuum.tileentity.OilRefinerTileEntity;

public class OilRefinerGuiContainer extends GuiContainer
{
    // Texture declaration
    private static final ResourceLocation CONTAINER_TEXTURE = new ResourceLocation(Automatuum.MOD_ID, "textures/gui/container/oil_refiner.png");

    // Fields
    private IInventory playerInventory;
    private OilRefinerTileEntity blockTileEntity;

    // Ctr
    public OilRefinerGuiContainer(EntityPlayer player, OilRefinerTileEntity tileEntity)
    {
        super(new OilRefinerContainer(player, tileEntity));
        this.playerInventory = player.inventory;
        this.blockTileEntity = tileEntity;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks)
    {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
    {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(CONTAINER_TEXTURE);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);

        // Progress stuff... you know, gauges.

    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
    {
        String s = "Oil Refiner";
        this.fontRenderer.drawString(s, this.xSize / 5 - this.fontRenderer.getStringWidth(s) / 2, 6, 0x404040);
        this.fontRenderer.drawString(this.playerInventory.getDisplayName().getUnformattedText(), 8, this.ySize - 96 + 2, 0x404040);
    }
}
