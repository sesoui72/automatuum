package sh.surge.enksoftware.automatuum.recipes;

import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.registries.IForgeRegistryEntry;

import javax.annotation.Nonnull;

public class OilRefinerRecipe extends IForgeRegistryEntry.Impl<OilRefinerRecipe>
{
    @Nonnull
    public final FluidStack input;
    @Nonnull
    public final FluidStack output;

    public final int ticks;
    public final int energyPerTick;

    public OilRefinerRecipe(@Nonnull FluidStack input, @Nonnull FluidStack output, int ticks, int energyPerTick)
    {
        this.input = input;
        this.output = output;
        this.ticks = ticks;
        this.energyPerTick = energyPerTick;
    }
}
