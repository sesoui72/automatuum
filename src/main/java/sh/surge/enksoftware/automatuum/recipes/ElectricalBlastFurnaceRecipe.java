package sh.surge.enksoftware.automatuum.recipes;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class ElectricalBlastFurnaceRecipe extends IForgeRegistryEntry.Impl<ElectricalBlastFurnaceRecipe>
{
    // Fields
    public final ItemStack out;
    public final Ingredient in1;
    public final Ingredient in2;
    public final Ingredient in3;
    public final Ingredient in4;
    public final Ingredient in5;
    public final float exp;
    public final int ticks;
    public final int energyPerTick;

    // Constructors
    public ElectricalBlastFurnaceRecipe(ItemStack input1, ItemStack input2, ItemStack input3, ItemStack output, int ticksToFinish, int energyPerTick, float experience)
    {
        this(input1, input2, input3, ItemStack.EMPTY, ItemStack.EMPTY, output, ticksToFinish, energyPerTick, experience);
    }

    public ElectricalBlastFurnaceRecipe(ItemStack input1, ItemStack input2, ItemStack input3, ItemStack input4, ItemStack output, int ticksToFinish, int energyPerTick, float experience)
    {
        this(input1, input2, input3, input4, ItemStack.EMPTY, output, ticksToFinish, energyPerTick, experience);
    }

    public ElectricalBlastFurnaceRecipe(ItemStack input1, ItemStack input2, ItemStack input3, ItemStack input4, ItemStack input5, ItemStack output, int ticksToFinish, int energyPerTick, float experience)
    {
        this(Ingredient.fromStacks(input1), Ingredient.fromStacks(input2), Ingredient.fromStacks(input3), Ingredient.fromStacks(input4), Ingredient.fromStacks(input5), output, ticksToFinish, energyPerTick, experience);
    }

    public ElectricalBlastFurnaceRecipe(Ingredient input1, Ingredient input2, Ingredient input3, Ingredient input4, Ingredient input5, ItemStack output, int ticksToFinish, int energyPerTick, float experience)
    {
        this.in1 = input1;
        this.in2 = input2;
        this.in3 = input3;
        this.in4 = input4;
        this.in5 = input5;
        this.out = output;
        this.exp = experience;
        this.ticks = ticksToFinish;
        this.energyPerTick = energyPerTick;
    }
}
