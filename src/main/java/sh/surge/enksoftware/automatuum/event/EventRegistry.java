package sh.surge.enksoftware.automatuum.event;

import net.minecraft.item.ItemStack;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import sh.surge.enksoftware.automatuum.items.ItemRegistry;

import java.util.Random;

import static net.minecraft.init.Blocks.COAL_ORE;

@Mod.EventBusSubscriber
public class EventRegistry
{
    private static Random dropChanceRand = new Random();

    @SubscribeEvent
    public static void registerDropEvents(BlockEvent.HarvestDropsEvent event)
    {

        if (event.getState().getBlock() == COAL_ORE && !event.isSilkTouching())
        {
            int dropChance = MathHelper.getInt(dropChanceRand, 0, 5);

            if (dropChance > 3)
            {
                event.getDrops().add(new ItemStack(ItemRegistry.CARBON));
            }
        }
    }
}
