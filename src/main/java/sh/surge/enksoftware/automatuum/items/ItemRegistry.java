package sh.surge.enksoftware.automatuum.items;

import sh.surge.enksoftware.automatuum.AutomatuumTab;
import sh.surge.enksoftware.automatuum.util.MatRefs;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.Items;
import net.minecraft.item.*;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;


@GameRegistry.ObjectHolder("automatuum")
@Mod.EventBusSubscriber(modid = "automatuum")
public class ItemRegistry
{
    // Laranium
    public static final Item LARANIUM_INGOT = Items.AIR;
    public static final Item LARANIUM_NUGGET = Items.AIR;
    public static final Item LARANIUM_AXE = Items.AIR;
    public static final Item LARANIUM_PICKAXE = Items.AIR;
    public static final Item LARANIUM_SHOVEL = Items.AIR;
    public static final Item LARANIUM_SWORD = Items.AIR;

    // Amethyte
    public static final Item AMETHYTE_INGOT = Items.AIR;
    public static final Item AMETHYTE_NUGGET = Items.AIR;
    public static final Item AMETHYTE_AXE = Items.AIR;
    public static final Item AMETHYTE_PICKAXE = Items.AIR;
    public static final Item AMETHYTE_SHOVEL = Items.AIR;
    public static final Item AMETHYTE_SWORD = Items.AIR;

    // Toparite
    public static final Item TOPARITE_INGOT = Items.AIR;
    public static final Item TOPARITE_NUGGET = Items.AIR;
    public static final Item TOPARITE_AXE = Items.AIR;
    public static final Item TOPARITE_PICKAXE = Items.AIR;
    public static final Item TOPARITE_SHOVEL = Items.AIR;
    public static final Item TOPARITE_SWORD = Items.AIR;

    // Hearanium
    public static final Item HEARANIUM_INGOT = Items.AIR;
    public static final Item HEARANIUM_NUGGET = Items.AIR;
    public static final Item HEARANIUM_AXE = Items.AIR;
    public static final Item HEARANIUM_PICKAXE = Items.AIR;
    public static final Item HEARANIUM_SHOVEL = Items.AIR;
    public static final Item HEARANIUM_SWORD = Items.AIR;

    // Sapphirite
    public static final Item SAPPHIRITE_INGOT = Items.AIR;
    public static final Item SAPPHIRITE_NUGGET = Items.AIR;
    public static final Item SAPPHIRITE_AXE = Items.AIR;
    public static final Item SAPPHIRITE_PICKAXE = Items.AIR;
    public static final Item SAPPHIRITE_SHOVEL = Items.AIR;
    public static final Item SAPPHIRITE_SWORD = Items.AIR;

    // Crystal
    public static final Item CRYSTAL_FRAGMENT = Items.AIR;
    public static final Item CRYSTAL_FRAME_FRAGMENT = Items.AIR;

    // Feros
    public static final Item FEROS_STICK = Items.AIR;
    public static final Item FEROS_CHUNK = Items.AIR;
    public static final Item FEROS_AXE = Items.AIR;
    public static final Item FEROS_PICKAXE = Items.AIR;
    public static final Item FEROS_SHOVEL = Items.AIR;
    public static final Item FEROS_SWORD = Items.AIR;

    // Misc. (Including sieves)
    public static final Item OBSIDIAN_STICK = Items.AIR;
    public static final Item HARSH_REFINING_SIEVE = Items.AIR;
    public static final Item FINE_REFINING_SIEVE = Items.AIR;
    public static final Item FINER_REFINING_SIEVE = Items.AIR;
    public static final Item FINEST_REFINING_SIEVE = Items.AIR;
    public static final Item STEEL_PLATE = Items.AIR;
    public static final Item CARBON = Items.AIR;

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event)
    {
        // Laranium Items
        Item laranium_ingot = new Item().setRegistryName("laranium_ingot").setTranslationKey("automatuum.laranium_ingot").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
        event.getRegistry().register(laranium_ingot);
        MatRefs.LARANIUM.setRepairItem(new ItemStack(laranium_ingot));
        event.getRegistry().register(new Item().setRegistryName("laranium_nugget").setTranslationKey("automatuum.laranium_nugget").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new AxeBase("laranium_axe", MatRefs.LARANIUM, 5.325F, -3.1F).setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new PickaxeBase("laranium_pickaxe", MatRefs.LARANIUM).setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSpade(MatRefs.LARANIUM).setRegistryName("laranium_shovel").setTranslationKey("automatuum.laranium_shovel").setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSword(MatRefs.LARANIUM).setRegistryName("laranium_sword").setTranslationKey("automatuum.laranium_sword").setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));

        // Amethyte Items
        Item amethyte_ingot = new Item().setRegistryName("amethyte_ingot").setTranslationKey("automatuum.amethyte_ingot").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
        event.getRegistry().register(amethyte_ingot);
        MatRefs.AMETHYTE.setRepairItem(new ItemStack(amethyte_ingot));
        event.getRegistry().register(new Item().setRegistryName("amethyte_nugget").setTranslationKey("automatuum.amethyte_nugget").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new AxeBase("amethyte_axe", MatRefs.AMETHYTE, 6.5F, -3.1F).setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new PickaxeBase("amethyte_pickaxe", MatRefs.AMETHYTE).setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSpade(MatRefs.AMETHYTE).setRegistryName("amethyte_shovel").setTranslationKey("automatuum.amethyte_shovel").setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSword(MatRefs.AMETHYTE).setRegistryName("amethyte_sword").setTranslationKey("automatuum.amethyte_sword").setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));

        // Toparite
        Item toparite_ingot = new Item().setRegistryName("toparite_ingot").setTranslationKey("automatuum.toparite_ingot").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
        event.getRegistry().register(toparite_ingot);
        MatRefs.TOPARITE.setRepairItem(new ItemStack(toparite_ingot));
        event.getRegistry().register(new Item().setRegistryName("toparite_nugget").setTranslationKey("automatuum.toparite_nugget").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new AxeBase("toparite_axe", MatRefs.TOPARITE, 6.0F, -3.1F).setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new PickaxeBase("toparite_pickaxe", MatRefs.TOPARITE).setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSpade(MatRefs.TOPARITE).setRegistryName("toparite_shovel").setTranslationKey("automatuum.toparite_shovel").setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSword(MatRefs.TOPARITE).setRegistryName("toparite_sword").setTranslationKey("automatuum.toparite_sword").setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));

        // Hearanium Items
        Item hearanium_ingot = new Item().setRegistryName("hearanium_ingot").setTranslationKey("automatuum.hearanium_ingot").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
        event.getRegistry().register(hearanium_ingot);
        MatRefs.HEARANIUM.setRepairItem(new ItemStack(hearanium_ingot));
        event.getRegistry().register(new Item().setRegistryName("hearanium_nugget").setTranslationKey("automatuum.hearanium_nugget").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new AxeBase("hearanium_axe", MatRefs.HEARANIUM, 5.7F, -3.1F).setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new PickaxeBase("hearanium_pickaxe", MatRefs.HEARANIUM).setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSpade(MatRefs.HEARANIUM).setRegistryName("hearanium_shovel").setTranslationKey("automatuum.hearanium_shovel").setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSword(MatRefs.HEARANIUM).setRegistryName("hearanium_sword").setTranslationKey("automatuum.hearanium_sword").setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));

        // Sapphirite Items
        Item sapphirite_ingot = new Item().setRegistryName("sapphirite_ingot").setTranslationKey("automatuum.sapphirite_ingot").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
        event.getRegistry().register(sapphirite_ingot);
        MatRefs.SAPPHIRITE.setRepairItem(new ItemStack(sapphirite_ingot));
        event.getRegistry().register(new Item().setRegistryName("sapphirite_nugget").setTranslationKey("automatuum.sapphirite_nugget").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new AxeBase("sapphirite_axe", MatRefs.SAPPHIRITE, 5.0F, -3.1F).setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new PickaxeBase("sapphirite_pickaxe", MatRefs.SAPPHIRITE).setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSpade(MatRefs.SAPPHIRITE).setRegistryName("sapphirite_shovel").setTranslationKey("automatuum.sapphirite_shovel").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSword(MatRefs.SAPPHIRITE).setRegistryName("sapphirite_sword").setTranslationKey("automatuum.sapphirite_sword").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));

        // Feros Items
        Item feros_chunk = new Item().setRegistryName("feros_chunk").setTranslationKey("automatuum.feros_chunk").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB);
        event.getRegistry().register(feros_chunk);
        MatRefs.FEROS.setRepairItem(new ItemStack(feros_chunk));
        event.getRegistry().register(new AxeBase("feros_axe", MatRefs.FEROS, 5.5F, -3.1F).setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new PickaxeBase("feros_pickaxe", MatRefs.FEROS).setMaxStackSize(1).setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSpade(MatRefs.FEROS).setRegistryName("feros_shovel").setTranslationKey("automatuum.feros_shovel").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSword(MatRefs.FEROS).setRegistryName("feros_sword").setTranslationKey("automatuum.feros_sword").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new Item().setRegistryName("feros_stick").setTranslationKey("automatuum.feros_stick").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));

        // Crystal Items
        event.getRegistry().register(new Item().setRegistryName("crystal_fragment").setTranslationKey("automatuum.crystal_fragment").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new Item().setRegistryName("crystal_frame_fragment").setTranslationKey("automatuum.crystal_frame_fragment").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));

        // Items
        event.getRegistry().register(new Item().setRegistryName("obsidian_stick").setTranslationKey("automatuum.obsidian_stick").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSieve("harsh_refining_sieve").setTranslationKey("automatuum.harsh_refining_sieve").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSieve("fine_refining_sieve").setTranslationKey("automatuum.fine_refining_sieve").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSieve("finer_refining_sieve").setTranslationKey("automatuum.finer_refining_sieve").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new ItemSieve("finest_refining_sieve").setTranslationKey("automatuum.finest_refining_sieve").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new Item().setRegistryName("steel_plate").setTranslationKey("automatuum.steel_plate").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
        event.getRegistry().register(new Item().setRegistryName("carbon").setTranslationKey("automatuum.carbon").setCreativeTab(AutomatuumTab.AUTOMATUUM_TAB));
    }

    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    public static void registerItemModels(ModelRegistryEvent event)
    {
        // Laranium
        ModelLoader.setCustomModelResourceLocation(LARANIUM_INGOT, 0, new ModelResourceLocation(LARANIUM_INGOT.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(LARANIUM_NUGGET, 0, new ModelResourceLocation(LARANIUM_NUGGET.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(LARANIUM_AXE, 0, new ModelResourceLocation(LARANIUM_AXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(LARANIUM_PICKAXE, 0, new ModelResourceLocation(LARANIUM_PICKAXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(LARANIUM_SHOVEL, 0, new ModelResourceLocation(LARANIUM_SHOVEL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(LARANIUM_SWORD, 0, new ModelResourceLocation(LARANIUM_SWORD.getRegistryName(), "inventory"));

        // Amethyte
        ModelLoader.setCustomModelResourceLocation(AMETHYTE_INGOT, 0, new ModelResourceLocation(AMETHYTE_INGOT.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(AMETHYTE_NUGGET, 0, new ModelResourceLocation(AMETHYTE_NUGGET.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(AMETHYTE_AXE, 0, new ModelResourceLocation(AMETHYTE_AXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(AMETHYTE_PICKAXE, 0, new ModelResourceLocation(AMETHYTE_PICKAXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(AMETHYTE_SHOVEL, 0, new ModelResourceLocation(AMETHYTE_SHOVEL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(AMETHYTE_SWORD, 0, new ModelResourceLocation(AMETHYTE_SWORD.getRegistryName(), "inventory"));

        // Toparite
        ModelLoader.setCustomModelResourceLocation(TOPARITE_INGOT, 0, new ModelResourceLocation(TOPARITE_INGOT.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(TOPARITE_NUGGET, 0, new ModelResourceLocation(TOPARITE_NUGGET.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(TOPARITE_AXE, 0, new ModelResourceLocation(TOPARITE_AXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(TOPARITE_PICKAXE, 0, new ModelResourceLocation(TOPARITE_PICKAXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(TOPARITE_SHOVEL, 0, new ModelResourceLocation(TOPARITE_SHOVEL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(TOPARITE_SWORD, 0, new ModelResourceLocation(TOPARITE_SWORD.getRegistryName(), "inventory"));

        // Hearanium
        ModelLoader.setCustomModelResourceLocation(HEARANIUM_INGOT, 0, new ModelResourceLocation(HEARANIUM_INGOT.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(HEARANIUM_NUGGET, 0, new ModelResourceLocation(HEARANIUM_NUGGET.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(HEARANIUM_AXE, 0, new ModelResourceLocation(HEARANIUM_AXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(HEARANIUM_PICKAXE, 0, new ModelResourceLocation(HEARANIUM_PICKAXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(HEARANIUM_SHOVEL, 0, new ModelResourceLocation(HEARANIUM_SHOVEL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(HEARANIUM_SWORD, 0, new ModelResourceLocation(HEARANIUM_SWORD.getRegistryName(), "inventory"));

        // Sapphirite
        ModelLoader.setCustomModelResourceLocation(SAPPHIRITE_INGOT, 0, new ModelResourceLocation(SAPPHIRITE_INGOT.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(SAPPHIRITE_NUGGET, 0, new ModelResourceLocation(SAPPHIRITE_NUGGET.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(SAPPHIRITE_AXE, 0, new ModelResourceLocation(SAPPHIRITE_AXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(SAPPHIRITE_PICKAXE, 0, new ModelResourceLocation(SAPPHIRITE_PICKAXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(SAPPHIRITE_SHOVEL, 0, new ModelResourceLocation(SAPPHIRITE_SHOVEL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(SAPPHIRITE_SWORD, 0, new ModelResourceLocation(SAPPHIRITE_SWORD.getRegistryName(), "inventory"));

        // Feros
        ModelLoader.setCustomModelResourceLocation(FEROS_CHUNK, 0, new ModelResourceLocation(FEROS_CHUNK.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(FEROS_SWORD, 0, new ModelResourceLocation(FEROS_SWORD.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(FEROS_STICK, 0, new ModelResourceLocation(FEROS_STICK.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(FEROS_AXE, 0, new ModelResourceLocation(FEROS_AXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(FEROS_PICKAXE, 0, new ModelResourceLocation(FEROS_PICKAXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(FEROS_SHOVEL, 0, new ModelResourceLocation(FEROS_SHOVEL.getRegistryName(), "inventory"));

        // Crystal
        ModelLoader.setCustomModelResourceLocation(CRYSTAL_FRAGMENT, 0, new ModelResourceLocation(CRYSTAL_FRAGMENT.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(CRYSTAL_FRAME_FRAGMENT, 0, new ModelResourceLocation(CRYSTAL_FRAME_FRAGMENT.getRegistryName(), "inventory"));

        // Items
        ModelLoader.setCustomModelResourceLocation(OBSIDIAN_STICK, 0, new ModelResourceLocation(OBSIDIAN_STICK.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(STEEL_PLATE, 0, new ModelResourceLocation(STEEL_PLATE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(HARSH_REFINING_SIEVE, 0, new ModelResourceLocation(HARSH_REFINING_SIEVE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(FINE_REFINING_SIEVE, 0, new ModelResourceLocation(FINE_REFINING_SIEVE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(FINER_REFINING_SIEVE, 0, new ModelResourceLocation(FINER_REFINING_SIEVE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(FINEST_REFINING_SIEVE, 0, new ModelResourceLocation(FINEST_REFINING_SIEVE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(CARBON, 0, new ModelResourceLocation(CARBON.getRegistryName(), "inventory"));

    }

}
