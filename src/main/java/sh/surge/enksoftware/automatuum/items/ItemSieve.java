package sh.surge.enksoftware.automatuum.items;

import net.minecraft.item.Item;

public class ItemSieve extends Item
{
    public ItemSieve(String name)
    {
        setRegistryName(name);
        setTranslationKey("automatuum." + name);
        setMaxStackSize(16);
    }
}
